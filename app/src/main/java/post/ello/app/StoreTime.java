package post.ello.app;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by BMusyoki on 1/12/2017.
 */

public class StoreTime {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;

    private static final String SHARED_PREFER_FILE_NAME = "timestamp";

    public StoreTime(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(SHARED_PREFER_FILE_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void storeTimeStamp(String timestamp) {
        editor.putString("timestamp", timestamp);
        editor.commit();
    }

    public String getTime(){
        String timestamp = pref.getString("timestamp", "0");
        return timestamp;
    }

    public void clear(){
        editor.clear();
        editor.commit();
    }
}
