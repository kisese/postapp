package post.ello.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

public class PostActivity extends AppCompatActivity {
    long startTime;
    long countUp;
    private Chronometer stopWatch;
    TextView timer_text;
    String sec_;
    Button back;
    private StoreTime storeTime;
    private int time_left;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        Intent i = getIntent();

        back = (Button) findViewById(R.id.back);
        back.setVisibility(View.GONE);
        time_left = i.getIntExtra("time_left", 0);

        timer_text = (TextView) findViewById(R.id.timer_text);
        stopWatch = (Chronometer) findViewById(R.id.timer);
        startTime = SystemClock.elapsedRealtime();
        stopWatch.setVisibility(View.GONE);

        if (!(i.getIntExtra("time_left", 0) == 0))
            startAlternativeCountDown(i.getIntExtra("time_left", 0));
        else
            startCountDown();
    }


    public void startCountDown() {
        new CountDownTimer(180000, 1000) {

            public void onTick(long millisUntilFinished) {
                String sec = String.valueOf(millisUntilFinished / 1000);
                Log.e("Timer", millisUntilFinished + " ");
                sec_ = sec;
                if (millisUntilFinished < 1800) {
                    timer_text.setText("0");
                    storeTime = new StoreTime(PostActivity.this);
                    storeTime.clear();
                } else
                    timer_text.setText(sec);
            }

            public void onFinish() {
                Intent i = new Intent(PostActivity.this, MainActivity.class);
                startActivity(i);
                stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer arg0) {
                        countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                        Log.e("count up", countUp + " ");
                        countUp = countUp - 5;
                        String asText = (countUp / 60) + ":" + (countUp % 60);
                        timer_text.setText(asText);
                    }
                });
            }
        }.start();
    }

    public void startAlternativeCountDown(int seconds) {
        int start_time = seconds * 1000;
        new CountDownTimer(start_time, 1000) {

            public void onTick(long millisUntilFinished) {
                String sec = String.valueOf(millisUntilFinished / 1000);
                Log.e("Timer", millisUntilFinished + " ");
                sec_ = sec;
                if (millisUntilFinished < 1800) {
                    timer_text.setText("0");
                    storeTime = new StoreTime(PostActivity.this);
                    storeTime.clear();
                } else
                    timer_text.setText(sec);
            }

            public void onFinish() {
                back.setVisibility(View.GONE);
                Intent i = new Intent(PostActivity.this, MainActivity.class);
                startActivity(i);
                stopWatch.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
                    @Override
                    public void onChronometerTick(Chronometer arg0) {
                        countUp = (SystemClock.elapsedRealtime() - arg0.getBase()) / 1000;
                        Log.e("count up", countUp + " ");
                        countUp = countUp - 5;
                        String asText = (countUp / 60) + ":" + (countUp % 60);
                        timer_text.setText(asText);
                    }
                });
            }
        }.start();
    }

    public void saveTime() {
        StoreTime storeTime = new StoreTime(PostActivity.this);
        storeTime.storeTimeStamp(sec_);
        Log.e("Saved Time", sec_);
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveTime();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveTime();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(PostActivity.this, "You have to wait 180s", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(PostActivity.this, PostActivity.class);
        i.putExtra("time_left", Integer.valueOf(sec_));
        Log.e("sec", sec_);
        startActivity(i);
    }
}
