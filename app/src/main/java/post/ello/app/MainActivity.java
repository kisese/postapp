package post.ello.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class MainActivity extends AppCompatActivity {

    EditText write_url, put_number;
    Button send, info;
    String number, text_;
    long startTime;
    private AdView mAdView;
    StoreTime storeTime;
    String seconds_left;
    private String param1 = " ", param2  = " ", param3 =" " , param4 = " ", param5 = " ", param6 = " ", param7 = " ", param8 = " ",  
            param9 = " ", param10 = " ", url = "http://";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        storeTime = new StoreTime(MainActivity.this);
        seconds_left = storeTime.getTime();

        //
        if (Integer.parseInt(seconds_left) > 0) {
            Intent i = new Intent(MainActivity.this, PostActivity.class);
            i.putExtra("time+left", Integer.parseInt(seconds_left));
            startActivity(i);
        }

        mAdView = (AdView) findViewById(R.id.adView);

        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // Check the LogCat to get your test device ID
                .addTestDevice("705C3D5DBC14D8EADD3B9107816BD501")
                .build();

        // Load ads into Interstitial Ad
        mAdView.loadAd(adRequest);

        info = (Button) findViewById(R.id.info_btn);
        write_url = (EditText) findViewById(R.id.write_url);
        put_number = (EditText) findViewById(R.id.put_number);
        send = (Button) findViewById(R.id.send);

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getValues();
            }
        });

        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(i);
            }
        });
    }

    public void getValues() {
        url = write_url.getText().toString();
        number = put_number.getText().toString();
        
        param4 = url;
        param5 = number;

        if (TextUtils.isEmpty(url)) {
            write_url.setError("Type in a url");
        } else {
            if (TextUtils.isEmpty(number)) {
                put_number.setError("Type in a number");
            } else {
                if (Integer.parseInt(number) < 100) {
                    put_number.setError("Type in a number above 100");
                } else if (Integer.parseInt(number) > 300) {
                    put_number.setError("Type in a number between 100 and 300");
                } else {
                    Toast.makeText(MainActivity.this, "Posting...", Toast.LENGTH_LONG).show();
                    new PostTask(param1, param2, param3, param4, param5, param6, param7, param8, param9, param10).execute();
                }
            }
        }
    }

    class PostTask extends AsyncTask<String, Void, String> {

        private Exception exception;
        private String param1, param2, param3, param4, param5, url, param6, param7, param8, param9, param10;

        public PostTask(String param1, String param2, String param3, String param4, String param5, String param6, String param7,
        String param8, String param9, String param10) {
            this.url = param4;
            this.param1 = param1;
            this.param2 = param2;
            this.param3 = param3;
            this.param4 = param4;
            this.param5 = param5;
             this.param6 = param6;
              this.param7 = param7;
               this.param8 = param8;
                this.param9 = param9;
                 this.param10 = param10;
        }

        protected String doInBackground(String... urls) {
            try {
                try {
                    String data = URLEncoder.encode("param1", "UTF-8")
                            + "=" + URLEncoder.encode(param1, "UTF-8");

                    data += "&" + URLEncoder.encode("param2", "UTF-8") + "="
                            + URLEncoder.encode(param2, "UTF-8");

                    data += "&" + URLEncoder.encode("param3", "UTF-8") + "="
                            + URLEncoder.encode(param3, "UTF-8");

                    data += "&" + URLEncoder.encode("orderUrl", "UTF-8") + "="
                            + URLEncoder.encode(param4, "UTF-8");

                    data += "&" + URLEncoder.encode("orderQuantity", "UTF-8") + "="
                            + URLEncoder.encode(param5, "UTF-8");
                            
                              data += "&" + URLEncoder.encode("param6", "UTF-8") + "="
                            + URLEncoder.encode(param6, "UTF-8");
                            
                              data += "&" + URLEncoder.encode("param7", "UTF-8") + "="
                            + URLEncoder.encode(param7, "UTF-8");
                            
                              data += "&" + URLEncoder.encode("param8", "UTF-8") + "="
                            + URLEncoder.encode(param8, "UTF-8");
                            
                              data += "&" + URLEncoder.encode("param9", "UTF-8") + "="
                            + URLEncoder.encode(param9, "UTF-8");
                            
                              data += "&" + URLEncoder.encode("param10", "UTF-8") + "="
                            + URLEncoder.encode(param10, "UTF-8");

                    String text = "" + url;
                    String post_url = url;
                    BufferedReader reader = null;
                    // Defined URL  where to send data
                    URL url = new URL(post_url);

                    // Send POST data request

                    URLConnection conn = url.openConnection();
                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(data);
                    wr.flush();

                    // Get the server response

                    reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = null;

                    // Read Server Response
                    while ((line = reader.readLine()) != null) {
                        // Append server response in string
                        sb.append(line + "\n");
                    }


                    text_ = sb.toString();

                    reader.close();

                    Log.e("Text", text);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                this.exception = e;

                return null;
            }
            return null;
        }


        protected void onPostExecute(String feed) {
            // TODO: check this.exception
            // TODO: do something with the feed
            Toast.makeText(MainActivity.this, text_, Toast.LENGTH_LONG).show();

            Intent i = new Intent(MainActivity.this, PostActivity.class);
            startActivity(i);
        }
    }

    @Override
    public void onPause() {
        if (mAdView != null) {
            mAdView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    public void onDestroy() {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
